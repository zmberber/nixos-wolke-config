{ config, options, pkgs, lib, ... }:
{
  imports =
    [ 
      ./hardware-configuration.nix
    ];
  system = {
    stateVersion = "22.11";
  };
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    growPartition = true;
    kernel.sysctl = {
      "net.ipv4.ip_forward" = 1;
      "net.ipv6.conf.all.forwarding" = 1;
    };
  };
  time = {
    timeZone = "Europe/Berlin";
  };
  networking = {
    hostName = "wolke";
    defaultGateway = {
      address = "202.61.236.1";
      interface = "ens3";
    };
    nameservers = [
      "9.9.9.9"
    ];
    interfaces.ens3 = {
      ipv4 = {
        addresses = [{
          address = "202.61.237.65";
          prefixLength = 22;
        }];
        routes = [{
          address = "202.61.236.1";
          prefixLength = 32;
        }];
      };
    };
    nat = {
      enable = true;
      externalInterface = "ens3";
      internalInterfaces = [ "wg0" ];
    };
    firewall = {
      allowedTCPPorts = [80 443];
      allowedUDPPorts = [51820];
    };
    wireguard.interfaces = {
      wg0 = {
        ips = [ "10.100.0.1/24" ];
        listenPort = 51820;
        postSetup = ''
        ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -s 10.100.0.0/24 -o ens3 -j MASQUERADE
      '';
        postShutdown = ''
        ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s 10.100.0.0/24 -o ens3 -j MASQUERADE
      '';
        privateKeyFile = "/etc/wireguard/private";
        peers = [
          {
            publicKey = "FTriNPbEr9cHIhfFgRtpbPRj2oWN5+Z2ZYf0ecrNMi4=";
            allowedIPs = [ "10.100.0.2/32" ];
          }
          {
            publicKey = "3iAgE128gyA1WSQ6PrudTrG6bVz2u0PopCGMXXaGC3I=";
            allowedIPs = [ "10.100.0.3/32" ];
          }
        ];
      };
    };
  };
  users = {
    users = {
      berber = {
        isNormalUser = true;
        extraGroups = [
          "wheel"
        ];
        openssh.authorizedKeys.keys = [
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDJwzgy2DIbzDj46157xoYnyXtxMRgCJnQI5BElG5JWyshYGcFUpIorvGKL92OFO/N7jLUQ+GhGYcjAG8nEd9L/DaOZOlgLeKUnKzOKbtpOxqw2z5ftdM6GZPbG4iAV7BmFNeIrSG++zV8G8PGBtxxZ05l037PqzfqTQ/fMICX5xW4jkI2oB6KGinnk6pQB8ZTO0oFnGHS17ZLquGUQRno283BP5u6QTLmeCd1MWWTRa6eDz09bs+0OzUVbA6bp6InUACJ/SvbA6Pebs5OSOqLRy/wQIeyXUNtDUqTi6EayMI/cu2W3/qAioDyYnb+nQCk01oZoxVIsxTChEf+8UJKO0ACngoiDjLpA/iO1LZENFK7dyxuif7YoVuWbb46/DMQsgA+UCKvPnwFBVuaPGj5AsIMidUlHQFI0L6S7mMFVESVKVvyjcaghpbpwNQoJZH3thgDl2v+Z8sgEUW8ECzbLJPDr4WQcm8+bjjQO1gbPlrP32y8l5v3BbV9QBdy/GZfB7nzIpH9mf7kLzuxeVLq4+0EO1fKKdsNjdB/yL6O/7PzoNAtwEoSJ/LaxmEqtnBUwrQHlRi7wKCdCYwM7M0wGYdEQHO/eFEd6ObQM/x6+LtGeKT6nNMwv5ix4b77MYxeNhzZ3rAVazXBfPtRd+Zfgt41AvSl3u7qtu5MYx3bffQ== cardno:15 597 970"
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGL4SqRZxHe+zziJ/8Q8nFfav5VfYj1zQUxnrHtrI2N4 berber@schlepptop"
        ];
      };
    };
  };
  environment = {
    systemPackages = with pkgs; [
      emacs-nox
      git
      weechat
    ];
  };
  programs = {
    tmux.enable = true;
  };
  services = {
    nextcloud = {
      enable = true;
      package = pkgs.nextcloud26;
      hostName = "cloud.pokemisch.de";
      https = true;
      config = {
        dbtype = "pgsql";
        dbuser = "nextcloud";
        dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
        dbname = "nextcloud";
        adminpassFile = "${pkgs.writeText "adminpass" "changeme"}";
        defaultPhoneRegion = "DE";
      };
      extraOptions = {
        oidc_login_client_id = "cloud";
        oidc_login_client_secret = lib.strings.removeSuffix "\n" "${builtins.readFile "/var/lib/secrets/keycloak-oidc-secret-pokemisch"}";
        oidc_login_provider_url = "https://keycloak.pokemisch.de/realms/pokemisch";
        oidc_login_end_session_redirect = true;
        oidc_login_logout_url = "https://cloud.pokemisch.de/apps/oidc_login/oidc";
        oidc_login_auto_redirect = false;
        oidc_login_redir_fallback = true;
        overwriteprotocol = "https";
        allow_user_to_change_display_name = false;
        lost_password_link = "disabled";
        oidc_login_default_quota = "100000000";
        oidc_login_button_text = "Log in with OpenID";
        oidc_login_hide_password_form = false;
        oidc_login_use_id_token = false;
        oidc_login_attributes = {
          id = "preferred_username";
          name = "name";
          mail = "email";
          quota = "ownCloudQuota";
          home = "homeDirectory";
          ldap_uid = "uid";
          groups = "groups";
          login_filter = "realm_access_roles";
          photoURL = "picture";
          is_admin = "ownCloudAdmin";
        };
        # oidc_login_default_group = "oidc";
        # oidc_login_use_external_storage = false;
        oidc_login_scope = "openid profile";
        oidc_login_proxy_ldap = false; 
        oidc_login_disable_registration = false;
        # oidc_login_alt_login_page = "assets/login.php";
        oidc_login_tls_verify = true;
        oidc_create_groups = true;
        # oidc_login_webdav_enabled = false;
        # oidc_login_password_authentication = false;
        oidc_login_public_key_caching_time = 86400;
        oidc_login_min_time_between_jwks_requests = 10;
        oidc_login_well_known_caching_time = 86400;
        # oidc_login_update_avatar = false;
        # oidc_login_skip_proxy = false;
        # oidc_login_code_challenge_method = "";  
      };
      phpOptions = options.services.nextcloud.phpOptions.default // {
        # "opcache.jit" = "tracing";
        # "opcache.jit_buffer_size" = "100M";
        # recommended by nextcloud admin overview
        "opcache.interned_strings_buffer" = "16";
      };
    };
    keycloak = {
      enable = true;
      settings = {
        hostname = "keycloak.pokemisch.de";
        hostname-admin = "keycloak-admin.pokemisch.de";
        hostname-strict-backchannel = false;
        http-port = 8080;
        https-port = 8443;
        proxy = "edge";
      };
      initialAdminPassword = "hallobimmelbahn";
      database.passwordFile = "/var/lib/secrets/db_password";
    };
    nginx = {
      enable = true;
      recommendedProxySettings = true;
      virtualHosts.${config.services.nextcloud.hostName} = {
        forceSSL = true;
        enableACME = true;
      };
      virtualHosts.${config.services.keycloak.settings.hostname} = {
        forceSSL = true;
        enableACME = true;
        locations =
          lib.attrsets.genAttrs [
            "/js/"
            "/realms/"
            "/resources/"
            "/robots.txt"
          ]
            (i: { proxyPass = "http://localhost:${toString config.services.keycloak.settings.http-port}" + i; });
      };
      virtualHosts.${config.services.keycloak.settings.hostname-admin} = {
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          proxyPass = "http://localhost:${toString config.services.keycloak.settings.http-port}/";
          extraConfig =
            ''
    allow 10.100.0.0/24;
    deny all;
    ''; 
        };
      };
    };
    openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = false;
        KbdInteractiveAuthentication = false;
      };
    };
    postgresql = {
      enable = true;
      ensureDatabases = [
        "nextcloud"
      ];
      ensureUsers = [
        { name = "nextcloud";
          ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
        }
      ];
    };
  };
  systemd = {
    services = {
      "nextcloud-setup" = {
        requires = ["postgresql.service"];
        after = ["postgresql.service"];
      };
      weechat-berber = {
        description = "chat environment setup for berber";
        environment.WEECHAT_HOME = "\$HOME/.config/weechat";
        after = [ "network.target" ];
        wantedBy = [ "multi-user.target" ];

        restartIfChanged = false;
        
        serviceConfig = {
          User = "berber";
          RemainAfterExit = true;
          Type = "oneshot";
          ExecStart = "${pkgs.tmux}/bin/tmux -2 new-session -d -s weechat ${pkgs.weechat}/bin/weechat";
          ExecStop = "${pkgs.tmux}/bin/tmux kill-session -t weechat";
        };
      };
    };
  };
  security = {
    acme = {
      acceptTerms = true;
      defaults.email = "certs@zmberber.com";
    };
  };
}
